package org.example;

import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 31) - 15);
        }
        String[] surnamesArray = {"Adamson","Smith","Johnson","Williams","Brown","Jones","Walker"};
        List<String>surnames = new ArrayList<>();
        for (String surnames1 : surnamesArray){
            surnames.add(surnames1);
        }
        System.out.println("Фамілії : " + surnames);
        List<Integer> list = new ArrayList<>();
        for (int list1 : array) {
            list.add(list1);
        }
        List<String> surname = new ArrayList<>();
        System.out.println("Генератор рандомних чисел : " + list);
        list.stream().map(a -> a * a).forEach(System.out::println);
        int sum = list.stream().map(a -> a * a).reduce(0, (a, b) -> a + b);
        System.out.println("Сума квадратів : " + sum);
        System.out.println("Парні числа : ");
        list.stream().filter(i -> i % 2 == 0).forEach(System.out::println);
        surnames.stream().filter(J -> J.contains("J")).forEach(System.out::print);

    }

}